package main

import (
	"golang.org/x/net/context"
	storage "google.golang.org/api/storage/v1"
	"io"
	"log"
	"net/http"
	"os"
)

// BucketObject addresses an object by Bucket name and Object name in
// Google Cloud Storage.
type BucketObject struct {
	Bucket string `json:"bucket"`
	Object string `json:"object"`
}

// Put uploads the source file to the BucketObject.
func (obj *BucketObject) Put(c context.Context, source, cacheControl string, client *http.Client) error {

	log.Printf("BucketObject.Put %v", obj)

	storageService, err := storage.New(client)
	if err != nil {
		log.Println("Error creating storage service.", err)
		return err
	}

	sourceFile, err := os.Open(source)
	if err != nil {
		log.Printf("Error opening source file %v. %v", source, err)
		return err
	}
	defer sourceFile.Close()

	fileInfo, err := sourceFile.Stat()
	if err != nil {
		log.Printf("Error stating file %v. %v", source, err)
		return err
	}

	object := &storage.Object{
		Name:         obj.Object,
		CacheControl: cacheControl,
	}
	_, err = storageService.Objects.Insert(obj.Bucket, object).ResumableMedia(c, sourceFile, fileInfo.Size(), "").Do()
	if err != nil {
		log.Printf("Error uploading source %v. %v", source, err)
		return err
	}

	return nil
}

// Get downloads the BucketObject to the destination.
func (obj *BucketObject) Get(c context.Context, destination string, client *http.Client) error {

	log.Printf("BucketObject.Get %v", obj)

	storageService, err := storage.New(client)
	if err != nil {
		log.Println("Error creating storage service.", err)
		return err
	}

	downloadResponse, err := storageService.Objects.Get(obj.Bucket, obj.Object).Download()
	if err != nil {
		log.Println("Error downloading source image.", err)
		return err
	}
	defer downloadResponse.Body.Close()

	destFile, err := os.Create(destination)
	if err != nil {
		log.Println("Error creating source image file.", err)
		return err
	}
	defer destFile.Close()

	_, err = io.Copy(destFile, downloadResponse.Body)
	if err != nil {
		log.Println("Error download response body.", err)
		return err
	}

	return nil
}

// String gets a gs URL suitable for use with the gsutil command.
func (obj *BucketObject) String() string {
	return "gs://" + obj.Bucket + "/" + obj.Object
}
