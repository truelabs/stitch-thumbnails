package main

import (
	"io/ioutil"
	"os"
	"testing"
)

func TestResize(t *testing.T) {
	file, err := ioutil.TempFile("/tmp", "resizetest")
	if err != nil {
		t.Fatal("error creating temp file.", err)
	}
	file.Close()
	defer os.Remove(file.Name())

	resize := &Resize{Width: 10, Height: 10}
	err = resize.Transform("test.jpg", file.Name())
	if err != nil {
		t.Error("resize transform failed", err)
	}
}
