package main

import (
	"testing"
)

func TestOperation(t *testing.T) {
	o := &Operation{
		Op: "resize",
		Parameters: map[string]interface{}{
			"width":  float64(20),
			"height": float64(30),
		},
	}

	transformer, err := o.FileTransformer()
	if err != nil {
		t.Fatal("didn't get transformer", err)
	}
	if transformer == nil {
		t.Fatal("err == nil but so is transformer")
	}
}

func TestInvalidOp(t *testing.T) {
	o := &Operation{Op: "invalid"}

	transformer, err := o.FileTransformer()
	if err != ErrInvalidOp {
		t.Errorf("expected %v but got %v", ErrInvalidOp, err)
	}
	if transformer != nil {
		t.Error("expected nil transformer but got", transformer)
	}
}
