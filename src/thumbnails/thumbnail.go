package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"github.com/drichardson/appengine/signedrequest"
	"github.com/drichardson/retry"
	"golang.org/x/net/context"
	storage "google.golang.org/api/storage/v1"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"time"
)

// ThumbnailOperations define a list of operations to apply to a source image.
type ThumbnailOperations struct {
	Source     BucketObject                `json:"source"`
	Operations []Operation                 `json:"operations"`
	Callback   signedrequest.SignedRequest `json:callback"`
}

type ThumbnailResponse struct {
	Width  int          `json:"width"`
	Height int          `json:"height"`
	Object BucketObject `json:"object"`
}

type ThumbnailOperationsResponse struct {
	Thumbnails []ThumbnailResponse `json:"thumbnails"`
}

// ThumbnailClientScopes returns the list of authentication scopes GenerateThumbnails
// needs to operate. This should be passed to google.DefaultClient.
func ThumbnailClientScopes() []string {
	return []string{storage.DevstorageReadWriteScope}
}

// Error codes for GenerateThumbnails
var ErrFailureResponse = errors.New("Failure Response")

// GenerateThumbnails generates the thumbnails defines by Operations and issues the
// Callback on success. This method takes a client to facilitate http.Client reuse,
// as recommended in http.Client's documentation.
func (thumbOps *ThumbnailOperations) GenerateThumbnails(c context.Context, client *http.Client, cacheControl string) error {

	log.Printf("GenerateThumbnails for %v", thumbOps.Source)

	// Create a temporary working directory for all files in this operation
	// so it's easy to clean up when we're done.
	tmpDir, err := ioutil.TempDir("/tmp", "thumbnails")
	if err != nil {
		log.Println("Failed to create temp directory.", err)
		return err
	}
	defer func() {
		if err := os.RemoveAll(tmpDir); err != nil {
			log.Printf("Error removing tmp directory %v. Error: %v", tmpDir, err)
		}
	}()

	// Get the source image from Google Cloud Storage.
	source := filepath.Join(tmpDir, "sourceimage")
	err = thumbOps.Source.Get(c, source, client)
	if err != nil {
		log.Printf("Failed to retreive source %v. %v", thumbOps.Source, err)
		return err
	}

	// Process the transform operations

	type result struct {
		err             error
		transformResult *TransformResult
		object          *BucketObject
	}

	operationResult := make(chan result)

	for i, op := range thumbOps.Operations {
		// shadow loop variables for closure
		i := i
		op := op

		perform := func() (*TransformResult, *BucketObject, error) {
			thumbnail := filepath.Join(tmpDir, "thumbnail-"+strconv.Itoa(i))
			log.Printf("thumbnail file: %v", thumbnail)

			transformer, err := op.FileTransformer()
			if err != nil {
				log.Println("Error getting FileTransformer from op.", err)
				return nil, nil, err
			}
			transformResult, err := transformer.Transform(source, thumbnail)
			if err != nil {
				log.Printf("Error executing operation %v. %v", op, err)
				return nil, nil, err
			}

			log.Printf("Transform result is %v", transformResult)

			// Upload the result to Google Cloud Storage
			newObject := &BucketObject{
				Bucket: op.Destination.Bucket,
				Object: op.Destination.Object,
			}
			err = newObject.Put(c, thumbnail, cacheControl, client)
			if err != nil {
				log.Printf("Error uploading thumbnail %v to %v", thumbnail, newObject)
				return nil, nil, err
			}
			return transformResult, newObject, nil
		}

		go func() {
			transformResult, object, err := perform()
			operationResult <- result{
				err:             err,
				transformResult: transformResult,
				object:          object,
			}
		}()
	}

	var response ThumbnailOperationsResponse

	// Wait for operations to complete.
	var anyErr error
	for i := 0; i < len(thumbOps.Operations); i++ {
		r := <-operationResult
		if r.err != nil {
			log.Printf("One of the operations failed. %v", r.err)
			anyErr = r.err
			continue
		}
		thumbnail := ThumbnailResponse{
			Width:  r.transformResult.Width,
			Height: r.transformResult.Height,
			Object: *r.object,
		}
		response.Thumbnails = append(response.Thumbnails, thumbnail)
	}
	if anyErr != nil {
		return anyErr
	}

	responseBytes, err := json.Marshal(response)
	if err != nil {
		log.Println("Error marshalling response.", err)
		return err
	}

	// Issue the callback request.
	err = retry.BackoffRetryN(10, 10*time.Millisecond, 10*time.Second, func() error {
		callback, err := thumbOps.Callback.HTTPRequest(bytes.NewReader(responseBytes))
		callback.Header.Set("Content-Type", "application/json")
		if err != nil {
			log.Println("Error getting callback HTTP request.", err)
			return err
		}
		log.Println("Issuing callback", callback.Method, callback.URL)
		resp, err := http.DefaultClient.Do(callback)
		if err != nil {
			log.Println("Error making callback request.", err)
			return err
		}
		if !(resp.StatusCode >= 200 && resp.StatusCode < 300) {
			log.Printf("Didn't get successful status code. %v", resp.StatusCode)
			return ErrFailureResponse
		}
		return nil
	})

	return err
}
