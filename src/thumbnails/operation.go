package main

import (
	"errors"
	"fmt"
)

// Operation defines a transformation to be applied to a file.
type Operation struct {
	Destination BucketObject           `json:"destination"`
	Op          string                 `json:"op"`
	Parameters  map[string]interface{} `json:"parameters"`
}

// Error codes for FileTransformer
var (
	ErrInvalidOp = errors.New("ErrInvalidOp")
)

// FileTransformer returns the FileTransformer this operation defines
// or nil with error on failure.
func (op *Operation) FileTransformer() (FileTransformer, error) {
	switch op.Op {
	case "resize":
		width, ok := op.Parameters["width"]
		if !ok {
			return nil, fmt.Errorf("Missing width")
		}
		height, ok := op.Parameters["height"]
		if !ok {
			return nil, fmt.Errorf("Missing height")
		}
		widthInt, ok := width.(float64)
		if !ok {
			return nil, fmt.Errorf("width is not an int64")
		}
		heightInt, ok := height.(float64)
		if !ok {
			return nil, fmt.Errorf("height is not an int64")
		}
		return &Resize{MaxWidth: int(widthInt), MaxHeight: int(heightInt)}, nil
	default:
		return nil, ErrInvalidOp
	}
}
