package main

import (
	"encoding/json"
	"flag"
	"github.com/drichardson/appengine/pullqueue"
	"golang.org/x/net/context"
	"golang.org/x/oauth2/google"
	"log"
	"time"
)

var project = flag.String("project", "stitch-1026", "Google Project ID")
var queueName = flag.String("queue", "thumbnail", "Taskqueue to consume.")
var leaseDuration = flag.Duration("lease-duration", 1*time.Minute, "Task lease duration.")
var dontLogTime = flag.Bool("dont-log-time", false, "If true, disable logging the time. Useful when run as systemd service.")
var cacheControl = flag.String("cache-control", "public, max-age=31536000", "Cache-Control header to use for thumbnails uploaded to Google Cloud Storage")

func main() {
	flag.Parse()
	logFlags := log.Lshortfile
	if !*dontLogTime {
		logFlags |= log.Ldate | log.Ltime
	}
	log.SetFlags(logFlags)

	c := context.Background()
	q := pullqueue.Queue{Project: *project, Name: *queueName}
	opts := pullqueue.Options{LeaseDuration: *leaseDuration, NoItemsLoopDelay: 2 * time.Second}

	// Create and reuse client, per comment in doc for http.Client.
	client, err := google.DefaultClient(c, ThumbnailClientScopes()...)
	if err != nil {
		log.Fatalln("Failed to get default client", err)
	}

	q.Run(c, &opts, func(c context.Context, payload []byte) error {
		thumbnailOps := new(ThumbnailOperations)
		if err := json.Unmarshal(payload, thumbnailOps); err != nil {
			log.Printf("Error unmarshalling json from payload. Error: %v", err)
			return err
		}

		c, cancel := context.WithTimeout(c, 1*time.Minute)
		defer cancel()
		if err := thumbnailOps.GenerateThumbnails(c, client, *cacheControl); err != nil {
			log.Printf("GenerateThumbnails failed. Error: %v", err)
			return err
		}

		return nil
	})

	log.Fatal("Unexpected: end of main, should never get here.")
}
