package main

import (
	"errors"
	"fmt"
	"log"
	"os/exec"
	"strconv"
	"strings"
)

// FileTransformer transforms a source file into a destination file.
type FileTransformer interface {
	Transform(source, destination string) (*TransformResult, error)
}

// Resize operation to generate a single thumbnail.
type Resize struct {
	MaxWidth  int
	MaxHeight int
}

// TransformResult contains information about a successful Transform operation.
type TransformResult struct {
	Width  int
	Height int
}

// Error codes returned by Transform
var (
	ErrCommandFailure = errors.New("ErrCommandFailure")
)

// Transform resizes the source file and writes result to destination file. The
// file will only be resized smaller, never bigger.
func (resize *Resize) Transform(source, destination string) (*TransformResult, error) {
	// The '>' operator tells convert to resize smaller only.
	size := fmt.Sprintf("%dx%d>", resize.MaxWidth, resize.MaxHeight)
	output, err := exec.Command("convert", source, "-resize", size, "-auto-orient", "-strip", destination).CombinedOutput()
	if err != nil {
		log.Printf("Error resizing image. Error: %v. Output:\n%v", err, string(output))
		return nil, err
	}
	log.Println("Resized image. Output:\n", string(output))

	output, err = exec.Command("identify", "-format", "%w %h", destination).CombinedOutput()
	if err != nil {
		log.Printf("Error getting size of output image. Error %v. Output:\n%v", err, string(output))
		return nil, err
	}
	outputStr := string(output)
	log.Println("Ran identify. Output:\n", outputStr)

	fields := strings.Fields(outputStr)
	if len(fields) != 2 {
		log.Printf("Expected 2 fields from identity command but got %v", len(fields))
		return nil, ErrCommandFailure
	}

	w, err := strconv.Atoi(fields[0])
	if err != nil {
		log.Printf("Error converting width %v to integer. %v", fields[0], err)
		return nil, err
	}
	h, err := strconv.Atoi(fields[1])
	if err != nil {
		log.Printf("Error converting height %v to integer. %v", fields[1], err)
		return nil, err
	}

	tr := &TransformResult{
		Width:  w,
		Height: h,
	}
	return tr, nil
}

// String prints a description of a Resize struct.
func (resize *Resize) String() string {
	return fmt.Sprintf("MaxWidth=%v,MaxHeight=%v", resize.MaxWidth, resize.MaxHeight)
}
