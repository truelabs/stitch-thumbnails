# Stitch Thumbnails
Resize images stored in Google Cloud Storage.
Jobs are scheduled on an App Engine Pull Task Queue.

# Development
To run the service on a non-compute instance machine, you need to create
a JWT credential in Google Cloud Storage and then add the e-mail
address associated with this new credential to the stitch application's
thumbnail task queue as a reader.

Then, following the instructions on [Google Application Default Credentials](https://developers.google.com/identity/protocols/application-default-credentials)
to set the `GOOGLE_APPLICATION_CREDENTIALS` environment variable to point at the JWT file.
